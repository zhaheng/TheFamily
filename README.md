#TheFamily

## [We are 伐 木 累 ！](http://family.liuzy88.com/)
* 家族亲人信息管理
* 显示家族树
* 显示生日目录
* 公历农历转换
* 手机浏览一样棒

## 界面预览
- ![家族树](http://qiniu-liuzy.liuzy88.com/family1.jpg)
- ![生日墙](http://qiniu-liuzy.liuzy88.com/family2.jpg)
- ![信息列表](http://qiniu-liuzy.liuzy88.com/family3.jpg)
- ![添加界面](http://qiniu-liuzy.liuzy88.com/family4.jpg)
- ![查询亲人](http://qiniu-liuzy.liuzy88.com/family5.jpg)

## 联系我
* Name：Liuzy
* QQ：416657468
* WeiXin：secondsun
* Email：[13162165337@163.com](mailto:13162165337@163.com)

## 欢迎土豪点这里[打赏]
- ![微信](http://upload.liuzy88.com/weixinpay.png)
- ![QQ钱包](http://upload.liuzy88.com/qqpay.png)
- ![支付宝](http://upload.liuzy88.com/alipay.png)
