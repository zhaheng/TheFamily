define(function() {
	var ps = [];//查询结果persons
	var familyTree = {
		init : function(callback) {
			$('#keywords')[0].focus();
			familyTree.eventBind();
			familyTree.searchPerson(1);
		},
		eventBind : function() {
			$('#btn_search').click(function() {
				familyTree.searchPerson(1);
			});
		},
		searchPerson : function(page) {
			$.ajax({
				type : 'POST',
				url : './person/searchList.do',
				data : '&keywords=' + $('#keywords').val() + "&page=" + page + '&rows=5',
				dataType : 'json',
				success : function(req) {
					ps = req.data;
					var dataHtml = '';
					for (var i = 0; i < req.data.length; i++) {
						var p = req.data[i];
						var sex = p.sex == 'M' ? '男' : '女';
						var age = new Date().getFullYear() - new Date(p.birthday).getFullYear();
						dataHtml += '<a href="javascript:familyTree.showPersonCard('+p.id+');" class="list-group-item"><strong>'+p.name+'</strong> '+sex+' '+age+'岁</a>';
					}
					$('#list_person').html(dataHtml);
					familyTree.showPersonCard(ps[0].id);
				}
			});
		},
		showPersonCard : function(id) {
			for (var i in ps) {
				if (ps[i].id == id) {
					$('#name').html(ps[i].name);
					if (ps[i].deathday == null || ps[i].deathday == '') {
						$('#death').hide();
						if (ps[i].homeTell != '') {
							$('#homeTell').parent('p').show();
							$('#homeTell').html('<a href="tel:' + ps[i].homeTell + '">' + ps[i].homeTell + '</a>');
						} else {
							$('#homeTell').parent('p').hide();
						}
						if (ps[i].phoneNumber != '') {
							$('#phoneNumber').parent('p').show();
							$('#phoneNumber').html('<a href="tel:' + ps[i].phoneNumber + '">' + ps[i].phoneNumber + '</a>');
						} else {
							$('#phoneNumber').parent('p').hide();
						}
					} else {
						$('#death').show();
						$('#homeTell').parent('p').hide();
						$('#phoneNumber').parent('p').hide();
					}
					$('#sex').html(ps[i].sex == 'M' ? '男' : '女');
					var b = new Date(ps[i].birthday);
					var age = new Date().getFullYear() - b.getFullYear();
					$('#age').html(age + '岁');
					$('#spouse').html(ps[i].spouseId);
					var bYangStr = b.getFullYear() + '年' + (b.getMonth() + 1) + '月' + b.getDate() + '日';
					$('#yang').html(bYangStr);
					$('#nong').html(ps[i].bNongStr);
					break;
				}
			}
			familyTree.showPersonTree(id);
		},
		showPersonTree : function(id) {
			$('.family-tree').html('');
			$.ajax({
				type : 'POST',
				url : './family/tree.do',
				data : '&id=' + id,
				dataType : 'json',
				success : function(req) {
					$('.family-tree').html('<ul>' + req + '</ul>');
				}
			});
		}
	}
	return familyTree;
});
