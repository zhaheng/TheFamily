package com.liuzy.family.controller;

import java.io.IOException;
import java.util.Scanner;

import javax.servlet.http.HttpServletRequest;

/**
 * @author liuzy 
 * 2016年2月19日下午4:18:45
 */
public class BaseController {
	public Object render(Object o) {
		//System.out.println(FastJsonKit.toJSON(o));
		return o;
	}
	public static String getXmlData(HttpServletRequest request) {
		StringBuilder sb = new StringBuilder();
		try {
			Scanner scanner = new Scanner(request.getInputStream());
			while (scanner.hasNextLine()) {
				sb.append(scanner.nextLine());
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
}
