package com.liuzy.family.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.liuzy.family.entity.PersonEntity;

public interface PersonEntityMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(PersonEntity record);

	PersonEntity selectByPrimaryKey(Integer id);

	List<PersonEntity> selectAll();

	int updateByPrimaryKey(PersonEntity record);

	List<PersonEntity> findChildsByBabaId(String babaId);
	
	List<PersonEntity> findChildsByMamaId(String mamaId);

	Integer searchListCount(Map<String, Object> params);

	List<PersonEntity> searchList(Map<String, Object> params, RowBounds rowBounds);

	List<PersonEntity> listBirthdayByMonth(String month);
}