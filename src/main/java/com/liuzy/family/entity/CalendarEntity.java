package com.liuzy.family.entity;

public class CalendarEntity {
    private Integer id;

    private String yang;

    private String yangStr;

    private String weekday;

    private String nong;

    private String nongStr;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getYang() {
        return yang;
    }

    public void setYang(String yang) {
        this.yang = yang;
    }

    public String getYangStr() {
        return yangStr;
    }

    public void setYangStr(String yangStr) {
        this.yangStr = yangStr;
    }

    public String getWeekday() {
        return weekday;
    }

    public void setWeekday(String weekday) {
        this.weekday = weekday;
    }

    public String getNong() {
        return nong;
    }

    public void setNong(String nong) {
        this.nong = nong;
    }

    public String getNongStr() {
        return nongStr;
    }

    public void setNongStr(String nongStr) {
        this.nongStr = nongStr;
    }
}